cabal-version: 2.4
name:           gitlab-haskell
category:       Git
version:        0.1.5
synopsis:       A Haskell library for the GitLab web API
description:
            This Haskell library queries and updates the database of a GitLab instance using the GitLab web API: <https://docs.gitlab.com/ee/api/>
            .
            Run all GitLab actions with `runGitLab`:
            .
            > runGitLab :: (MonadUnliftIO m, MonadIO m)
            >  => GitLabServerConfig -- ^ the GitLab server details
            >  -> GitLab m a         -- ^ the GitLab action
            >  -> m a
            .
            For example:
            .
            > myTodos <- runGitLab
            >      (defaultGitLabServer
            >          { url = "https://gitlab.example.com"
            >          , token="my_token"} )
            >      todos
            .
            Which uses the `todos` function:
            .
            > todos :: MonadIO m => GitLab m [Todo] 
            .
            Unsurprisingly, this library is maintained on GitLab: <https://gitlab.com/robstewart57/gitlab-haskell>


homepage:       https://gitlab.com/robstewart57/gitlab-haskell
bug-reports:    https://gitlab.com/robstewart57/gitlab-haskell/issues
author:         Rob Stewart
maintainer:     robstewart57@gmail.com
copyright:      2019 Rob Stewart, Heriot-Watt University
license:        BSD-3-Clause
license-file:   LICENSE
build-type:     Simple
extra-source-files:
    README.md

source-repository head
  type: git
  location: https://gitlab.com/robstewart57/gitlab-haskell

library
  exposed-modules:
                  GitLab
                , GitLab.Types
                , GitLab.API.Groups
                , GitLab.API.Members
                , GitLab.API.Commits
                , GitLab.API.Projects
                , GitLab.API.Users
                , GitLab.API.Issues
                , GitLab.API.Pipelines
                , GitLab.API.Branches
                , GitLab.API.Jobs
                , GitLab.API.Repositories
                , GitLab.API.MergeRequests
                , GitLab.API.RepositoryFiles
                , GitLab.API.Todos
  other-modules:
                  GitLab.WebRequests.GitLabWebCalls
  hs-source-dirs:
                 src
  build-depends:
                base >=4.7 && <5
              , http-conduit
              , connection
              , aeson >= 1.4.4.0
              , bytestring
              , text
              , http-types
              , transformers
              , unliftio
              , unliftio-core
              , time
  default-language: Haskell2010
  ghc-options: -Wall
