{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

-- | internal module to support modules in GitLab.API
module GitLab.WebRequests.GitLabWebCalls
  (
    gitlab
  , gitlabWithAttrs
  , gitlabOne
  , gitlabWithAttrsOne
  , gitlabPost
  , gitlabReqText
  , gitlabReqByteString

  -- * low level API
  , ProjectID, Addr, Params
  , Exception(..)
  , inProject
  , getOne, getMany
  ) where

import qualified Control.Exception as Exception
import Network.HTTP.Conduit
import qualified Data.ByteString.Lazy as BSL
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.Aeson
import qualified Control.Exception as E
import Network.HTTP.Types.Status
import GitLab.Types
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class
import qualified Data.ByteString.Lazy.Char8 as C
import Network.HTTP.Types.URI


gitlabPost ::
  (MonadIO m, FromJSON b) =>
  Text -- ^ the URL to post to
  -> Text -- ^ the data to post
  -> GitLab m (Either Status b)
gitlabPost urlPath dataBody = do
  cfg <- serverCfg <$> ask
  manager <- httpManager <$> ask
  let url' = url cfg <> "/api/v4" <> urlPath
  let request' = parseRequest_ (T.unpack url')
      request = request'
                { method = "POST"
                , requestHeaders =
                  [("PRIVATE-TOKEN", T.encodeUtf8 (token cfg))]
                , requestBody = RequestBodyBS (T.encodeUtf8 dataBody) }
  res <- liftIO $ tryGitLab 0 request (retries cfg) manager Nothing
  case responseStatus res of
    resp@(Status 404 _msg) -> return (Left resp)
    resp@(Status 409 _msg) -> return (Left resp)
    _ -> return (case parseBSOne (responseBody res) of
                   Just x -> Right x
                   Nothing -> Left $
                     mkStatus 409 "unable to parse POST response")

tryGitLab ::
     Int -- ^ the current retry count
  -> Request -- ^ the GitLab request
  -> Int -- ^ maximum number of retries permitted
  -> Manager -- ^ HTTP manager
  -> Maybe HttpException -- ^ the exception to report if maximum retries met
  -> IO (Response BSL.ByteString)
tryGitLab i request maxRetries manager lastException
  | i == maxRetries = error (show lastException)
  | otherwise =
      httpLbs request manager
      `E.catch`
        \ex -> tryGitLab (i+1) request maxRetries manager (Just ex)

parseBSOne :: FromJSON a => BSL.ByteString -> Maybe a
parseBSOne bs =
  case eitherDecode bs of
    Left _err -> Nothing
      -- useful when debugging
      -- error (show _err)
    Right xs -> Just xs

parseBSMany :: FromJSON a => BSL.ByteString -> IO [a]
parseBSMany bs =
  case eitherDecode bs of
    Left s -> Exception.throwIO $ Exception s
    Right xs -> return xs

gitlabReqJsonMany :: (MonadIO m, FromJSON a) => Text -> Text -> GitLab m [a]
gitlabReqJsonMany urlPath attrs =
  go 1 []
  where
    go i accum = do
      cfg <- serverCfg <$> ask
      manager <- httpManager <$> ask
      let url' =
               url cfg
            <> "/api/v4"
            <> urlPath
            <> "?per_page=100"
            <> "&page="
            <> T.pack (show i)
            <> T.decodeUtf8 (urlEncode False (T.encodeUtf8 attrs))
      let request' = parseRequest_ (T.unpack url')
          request = request'
            { requestHeaders =
              [("PRIVATE-TOKEN", T.encodeUtf8 (token cfg))]
            , responseTimeout = responseTimeoutMicro (timeout cfg)
            }
      res <- liftIO $ tryGitLab 0 request (retries cfg) manager Nothing
      outs <- liftIO $ parseBSMany (responseBody res)
      if totalPages res == i
        then return $ concat (reverse accum)
        else go (i+1) (outs : accum)

gitlabReqOne :: (MonadIO m) => (BSL.ByteString -> Either String output) -> Text
  -> Text -> GitLab m output
gitlabReqOne parser urlPath attrs = go
  where
    go = do
      cfg <- serverCfg <$> ask
      manager <- httpManager <$> ask
      let url' =
               url cfg
            <> "/api/v4"
            <> urlPath
            <> "?per_page=100"
            <> "&page=1"
            <> attrs
      let request' = parseRequest_ (T.unpack url')
          request = request'
            { requestHeaders =
              [("PRIVATE-TOKEN", T.encodeUtf8 (token cfg))]
            , responseTimeout = responseTimeoutMicro (timeout cfg)
            }
      liftIO $ do
        res <- tryGitLab 0 request (retries cfg) manager Nothing
        either (Exception.throwIO . Exception) return $
          parser (responseBody res)

gitlabReqJsonOne :: (MonadIO m, FromJSON a) => Text -> Text -> GitLab m (Maybe a)
gitlabReqJsonOne = gitlabReqOne (Right . parseBSOne)

gitlabReqText :: (MonadIO m) => Text -> GitLab m String
gitlabReqText urlPath = gitlabReqOne (Right . C.unpack) urlPath ""

gitlabReqByteString :: (MonadIO m) => Text -> GitLab m BSL.ByteString
gitlabReqByteString urlPath = gitlabReqOne (Right . Prelude.id) urlPath ""

gitlab :: (MonadIO m, FromJSON a) => Text -> GitLab m [a]
gitlab addr = gitlabReqJsonMany addr ""

gitlabOne :: (MonadIO m, FromJSON a) => Text -> GitLab m (Maybe a)
gitlabOne addr = gitlabReqJsonOne addr ""

gitlabWithAttrs :: (MonadIO m, FromJSON a) => Text -> Text -> GitLab m [a]
gitlabWithAttrs  = gitlabReqJsonMany

gitlabWithAttrsOne :: (MonadIO m, FromJSON a) => Text -> Text -> GitLab m (Maybe a)
gitlabWithAttrsOne  = gitlabReqJsonOne

totalPages :: Response a -> Int
totalPages = maybe 1 (read . T.unpack . T.decodeUtf8) . lookup "X-Total-Pages"
  . responseHeaders


-- * low level API

type ProjectID = Int
type Addr = Text
type Params = [(Text, Text)]

newtype Exception = Exception String
  deriving (Eq, Show)
instance Exception.Exception Exception

inProject :: ProjectID -> Addr -> Addr
inProject projectId addr = "projects/" <> T.pack (show projectId) <> "/" <> addr

getOne :: (MonadIO m, FromJSON a) => Addr -> Params -> GitLab m a
getOne addr params =
  gitlabReqOne eitherDecode ("/" <> addr) (encodeParams params)

getMany :: (MonadIO m, FromJSON a) => Addr -> Params -> GitLab m [a]
getMany addr params = gitlabWithAttrs ("/" <> addr) (encodeParams params)

encodeParams :: Params -> Text
encodeParams params = mconcat $ do
  (k, v) <- params
  -- Internally gitlabWithAttrs prepends a ? for paging.
  return $ "&" <> k <> (if v == "" then "" else "=" <> v)
